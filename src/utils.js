if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}

module.exports = {
  returnFalse: function (){
    return false;
  },
  decimalToHex: function (d, pad) {
    var hex = BigNumber(d).toString(16);
    pad = pad || 0;

    while (hex.length < pad*2) {
        hex = "00" + hex;
    }

    return hex;
  },
  padAddress: function (address){
    return "000000000000000000000000" + address;
  },
  oneEtherInWei: Math.pow(10, 18)
}
