var m = require("mithril"),
    dapp = require("./components/dapp");

//initialize
m.route.mode = "pathname";
m.route(document.getElementById("dapp"), "/", {
    "/": dapp,
    "/browse": dapp,
    "/create": dapp,
    "/about": dapp,
    "/campaign/:campaignId": dapp,
});

// for debugging
window.m = m;
