var	contracts = require("../contracts.json"),
    utils = require("../utils");

function Campaign(id){
	this.id = id;
  this.contract = web3.eth.contract(window.registry.contracts(id), contracts.campaign.abi);
  this.name = this.contract.name();
  this.gistId = this.contract.gistId();
  this.beneficiary = this.contract.beneficiary();
  this.goal = this.contract.goal().toNumber()/utils.oneEtherInWei;
  this.raised = this.contract.raised().toNumber()/utils.oneEtherInWei;
  this.balance = web3.toDecimal(web3.eth.balanceAt(this.contract.address))/utils.oneEtherInWei;
  this.numFunders = this.contract.numFunders().toNumber();
  this.startBlock = this.contract.startBlock().toNumber();
}

Campaign.prototype.update = function(){
  this.goal = this.contract.call().goal().toNumber()/utils.oneEtherInWei;
  this.numFunders = this.contract.numFunders().toNumber();
  this.raised = this.contract.raised().toNumber()/utils.oneEtherInWei;
  this.balance = web3.toDecimal(web3.eth.balanceAt(this.contract.address))/utils.oneEtherInWei;
}

module.exports = Campaign;
