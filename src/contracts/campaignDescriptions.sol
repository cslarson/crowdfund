contract CampaignDescriptions {

  struct CampaignDescription {
    string32 name;
    string32 keywords;
  }

  function setCampaignsAddr(address contractAddr) {
    if (campaignsAddr == 0x0) {
      campaignsAddr = contractAddr;
    }
  }

  function createCampaignDescription(uint campaignID, string32 name, string32 keywords) {
    if (msg.sender != campaignsAddr) {
      return;
    }
    CampaignDescription cd = campaignDescriptions[campaignID];  // assigns reference
    cd.name = name;
    cd.keywords = keywords;
  }

  function getCampaignName(uint campaignID) constant returns (string32 name) {
    CampaignDescription cd = campaignDescriptions[campaignID];
    return cd.name;
  }

  function getCampaignKeywords(uint campaignID) constant returns (string32 keywords) {
    CampaignDescription cd = campaignDescriptions[campaignID];
    return cd.keywords;
  }

  function testContractCall() {
    if (msg.sender == campaignsAddr) {
      testcall = true;
    }
  }

  function test() constant returns (bool success) {
    return true;
  }

  // address private campaignsAddr;
  address public campaignsAddr;
  bool public testcall;
  mapping (uint => CampaignDescription) campaignDescriptions;
}
