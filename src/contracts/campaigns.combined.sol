contract Campaigns {

  struct Funder {
    address addr;
    uint amount;
  }

  struct Campaign {
    address beneficiary;
    uint goal;
    uint numFunders;
    uint amount;
    uint startTime;
    bool ended;
    string32 descriptionJSON;
    mapping (uint => Funder) funders;
  }

  function createCampaign(uint goal, string32 descriptionJSON) returns (uint campaignID) {
    campaignID = numCampaigns++; // campaignID is return variable
    Campaign c = campaigns[campaignID];  // assigns reference
    c.beneficiary = msg.sender;
    c.goal = goal;
    c.descriptionJSON = descriptionJSON;
    c.startTime = block.timestamp;
  }

  function contribute(uint campaignID) {
    Campaign c = campaigns[campaignID];
    Funder f = c.funders[c.numFunders++];
    f.addr = msg.sender;
    f.amount = msg.value;
    c.amount += f.amount;
  }

  function endCampaign(uint campaignID) returns (bool success) {
    Campaign c = campaigns[campaignID];
    if (c.ended == true || msg.sender != c.beneficiary || c.amount < c.goal)
      return false;
    c.beneficiary.send(c.amount);
    // c.amount = 0;
    c.ended = true;
    return true;
  }

  function getCampaignBeneficiary(uint campaignID) constant returns (address beneficiary) {
    Campaign c = campaigns[campaignID];
    return c.beneficiary;
  }

  function getCampaignDescriptionJSON(uint campaignID) constant returns (string32 descriptionJSON) {
    Campaign c = campaigns[campaignID];
    return c.descriptionJSON;
  }

  function getCampaignGoal(uint campaignID) constant returns (uint goal) {
    Campaign c = campaigns[campaignID];
    return c.goal;
  }

  function getCampaignAmount(uint campaignID) constant returns (uint amount) {
    Campaign c = campaigns[campaignID];
    return c.amount;
  }

  function getCampaignNumFunders(uint campaignID) constant returns (uint numFunders) {
    Campaign c = campaigns[campaignID];
    return c.numFunders;
  }

  function getCampaignStartTime(uint campaignID) constant returns (uint startTime) {
    Campaign c = campaigns[campaignID];
    return c.startTime;
  }

  function getCampaignEnded(uint campaignID) constant returns (bool ended) {
    Campaign c = campaigns[campaignID];
    return c.ended;
  }

  uint public numCampaigns;
  mapping (uint => Campaign) campaigns;

}
