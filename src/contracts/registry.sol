contract Registry {

  struct Contract {
    address addr;
  }

  function Registry() {
    numContracts = 0;
    manager = msg.sender;
  }

  function register(address contractAddr) {
    if (msg.sender == contractCreatorAddress) {
      uint contractId = numContracts++;
      Contract c = contracts[contractId];
      c.addr = contractAddr;
    }
  }

  function updateContractCreator(address newContractCreatorAddress){
    if (msg.sender == manager)
      contractCreatorAddress = newContractCreatorAddress;
  }

  function transferManagement(address newManager) {
    if (msg.sender == manager)
      manager = newManager;
  }

  function collect() {
    if (msg.sender == manager)
      manager.send(this.balance);
  }

  uint public numContracts;
  mapping (uint => Contract) public contracts;
  address public contractCreatorAddress;
  address public manager;
}
