contract Campaign {

  struct Funder {
    address addr;
    uint amount;
  }

  function Campaign(uint goalIn, string32 nameIn, string32 gistIdIn) {
    beneficiary = msg.sender;
    goal = goalIn;
    name = nameIn;
    gistId = gistIdIn;
    startBlock = block.number;  // don't do this timestamp not trustworthy
  }

  function () {
    if(msg.value > 0){
      uint funderId = numFunders++;
      Funder f = funders[funderId];
      f.addr = msg.sender;
      f.amount = msg.value;
      raised += f.amount;
    }
  }

  function collect() {
    if (msg.sender == beneficiary && raised >= goal)
      beneficiary.send(this.balance);
  }

  function refund(uint funderId) {
    Funder f = funders[funderId];
    if (msg.sender == f.addr && raised < goal) {
      f.addr.send(f.amount);
      raised -= f.amount;
      f.amount = 0;
    }
  }

  address public beneficiary;
  uint public goal;
  uint public numFunders;
  uint public raised;
  uint public startBlock;
  string32 public name;
  string32 public gistId;
  mapping (uint => Funder) public funders;

}
