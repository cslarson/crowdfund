var m = require("mithril");

var home = {};

home.controller = function() {

};

home.view = function(ctrl) {
  return [
    m("main.dapp-content.home", [
      m("h1", "home"),
      m("h2", "raise ether for your project"),
      m("ul", [
        m("li", m("h3", "browse campaigns")),
        m("li", m("h3", "create your own campaign")),
        m("li", m("h3", "learn how crowdfunding using this dapp works"))
      ])
    ])
  ];
};

module.exports = home;
