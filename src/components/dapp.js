var m = require("mithril"),
    utils = require("../utils"),
    contracts = require("../contracts.json"),
    campaign = require("./campaign"),
    Campaign = require("../constructors/Campaign"),
    about = require("./about"),
    home = require("./home"),
    browse = require("./browse"),
    create = require("./create");

dapp = {};
dapp.numCampaigns = m.prop();
dapp.campaigns = [];
dapp.registryAddr = "0x11aa02f73c43bb995ea25639e0554dc5209af9a5";
dapp.campaignCreatorAddr = "";

// var checkRegCode = web3.eth.codeAt(dapp.registryAddr);
// if(checkRegCode !== contracts.registry.hex) {
//   if(confirm("The current registry address does not contain a valid registry contract. Create one now?")){
//     // console.log(contracts.registry.hex);
//     dapp.registryAddr = web3.eth.transact({code: contracts.registry.hex});
//   } else {
//     alert("This dapp will not function fully without a valid campaign registry.");
//   }
// }
//
// var checkCampaignCreatorCode = web3.eth.codeAt(dapp.campaignCreatorAddr);
// if(!dapp.campaignCreatorAddr) {
//   if(confirm("The current campaignCreator address does not contain a valid contract creator. Create one now?")){
//     dapp.campaignCreatorAddr = web3.eth.transact({code: contracts.campaignCreator.hex + utils.padAddress(dapp.registryAddr.substring(2))});
//   } else {
//     alert("This dapp will not function fully without a valid campaign creator.");
//   }
// }

window.registry = web3.eth.contract(dapp.registryAddr, contracts.registry.abi);
window.campaignCreator = web3.eth.contract(dapp.campaignCreatorAddr, contracts.campaignCreator.abi);
window.registry.transact().updateContractCreator(dapp.campaignCreatorAddr);

var watch = web3.eth.watch("chain");

watch.changed(function(){
  console.log("chain");
  if(window.registry){
    dapp.numCampaigns(registry.numContracts().toNumber());
  }
  dapp.getAllCampaignData();
  m.redraw();
});

dapp.getAllCampaignData = function(){
  var numCampaigns = dapp.numCampaigns(),
      currId = numCampaigns - 1;
  while (currId >= 0){
    var contract;
    if(!dapp.campaigns[currId]){
      dapp.campaigns[currId] = new Campaign(currId);
      // contract = web3.eth.contract(registry.contracts(currId), contracts.campaign.abi);

      // dapp.campaigns[currId] = {
      //   id: currId,
      //   contract: contract,
      //   name: contract.name(),
      //   gistId: contract.gistId(),
      //   beneficiary: contract.beneficiary(),
      //   goal: contract.goal().toNumber()/utils.oneEtherInWei,
      //   raised: contract.raised().toNumber()/utils.oneEtherInWei,
      //   balance: web3.toDecimal(web3.eth.balanceAt(contract.address))/utils.oneEtherInWei,
      //   numFunders: contract.numFunders().toNumber(),
      //   startTime: contract.startTime().toNumber()
      // };
    } else {
      dapp.campaigns[currId].update();
      // contract = dapp.campaigns[currId].contract;
      // dapp.campaigns[currId].goal = contract.call().goal().toNumber()/utils.oneEtherInWei;
      // dapp.campaigns[currId].numFunders = contract.numFunders().toNumber();
      // dapp.campaigns[currId].raised = contract.raised().toNumber()/utils.oneEtherInWei;
      // dapp.campaigns[currId].balance = web3.toDecimal(web3.eth.balanceAt(contract.address))/utils.oneEtherInWei;
    }
    currId--;
  }
};

dapp.numCampaigns(registry.numContracts().toNumber());
dapp.getAllCampaignData();

//controller
dapp.controller = function() {
  this.route = m.route();

  var entry = this.route.split("/")[1];

  switch(entry){
    case "":
      this.contentModule = home;
      this.contentView = new home.controller();
      break;
    case "browse":
      this.contentModule = browse;
      this.contentView = new browse.controller(dapp.campaigns);
      break;
    case "create":
      this.contentModule = create;
      this.contentView = new create.controller();
      break;
    case "about":
      this.contentModule = about;
      this.contentView = new about.controller();
      break;
    case "campaign":
      var c = dapp.campaigns.find(function(c){
        return c.id === parseInt(m.route.param("campaignId"));
      });
      this.contentModule = campaign;
      this.contentView = new campaign.controller(c);
      break;
  }
  // console.log(m.route(), this.contentModule);
};

//view
dapp.view = function(ctrl) {
  // console.log(ctrl.route);
  return [
    m("header.dapp-header",[
      m("h1", m("a[href='/']", {config: m.route}, "Cool Crowdfunder Dapp Name")),
      m("nav", [
        m("ul", [
          m("li", [
            m("a[href='/browse']"+(ctrl.route === "/browse" ? ".active":""), {config: m.route}, "campaigns")
          ]),
          m("li", [
            m("a[href='/create']"+(ctrl.route === "/create" ? ".active":""), {config: m.route}, "create")
          ]),
          m("li", [
            m("a[href='/about']"+(ctrl.route === "/about" ? ".active":""), {config: m.route}, "about")
          ])
        ])
      ])
    ]),
    !window.registry ? m("button", {onclick: dapp.createRegistryAndWatch}, "create registry") :
    m(".dapp-flex-content", [
      ctrl.contentModule.view(ctrl.contentView)
    ])
  ];
};

module.exports = dapp;
