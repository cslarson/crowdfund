var m = require("mithril");

var about = {};

about.controller = function() {
};

about.view = function(ctrl) {
  return [
    m("main.dapp-content.about", [
      m("h1", "about this dapp"),
      m("dl", [
        m("dt", "registry address"),
        m("dd", registry.address),
        m("dt", "number of campaigns"),
        m("dd", dapp.numCampaigns())
      ]),
      m("h3", "for contributors"),
      m("ul", [
        m("li", "contributors may receive refunds until the campaign goal is reached. funder amounts of 0 indicate a refund.")
      ]),
      m("h3", "for campaign creators"),
      m("ul", [
        m("li", "campaign description and reports are in markdown."),
        m("li", "once goal is reached current and future campaign contract balance is unlocked.")
      ])
    ])
  ];
};

module.exports = about;
