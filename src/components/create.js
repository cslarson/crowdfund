var m = require("mithril"),
    contracts = require("../contracts.json"),
    utils = require("../utils");

var create = {};
create.name = m.prop("");
create.gistId = m.prop("");
create.goal = m.prop(0);
create.pending = false;
create.submit = function(){
  // var registryHex = utils.padAddress(registry.address.substring(2));
  // var goalHex = utils.decimalToHex(parseInt(create.goal())*utils.oneEtherInWei, 32);
  // var nameHex = web3.fromAscii(create.name(), 32).substring(2);
  // var gistIdHex = web3.fromAscii(create.gistId(), 32).substring(2);

  // var code = contracts.campaign.hex + registryHex + goalHex + nameHex + gistIdHex;

  // var addr = web3.eth.transact({code: code});

  var addr = window.campaignCreator.createCampaign(parseInt(create.goal())*utils.oneEtherInWei, create.name(), create.gistId())

  console.log("how to tell if transaction allowed?");
  create.pending = true;

  var watch = web3.eth.watch('chain');
  watch.changed(function (log) {
    if(web3.eth.stateAt(addr, "0x") === registry.address){
      var c = dapp.campaigns.find(function(campaign){
        return campaign.contract.address === addr;
      });
      if(c){
        console.log("campaign creation success");
        m.route("/campaign/"+c.id);
        watch.uninstall();
      }
    }
  });

};

create.controller = function() {
  // console.log("create campaigns controller")
};

create.view = function(ctrl) {
  return [
    m("main.dapp-content.create", [
      m("h1", "create a new campaign"),
      create.pending ? m("", "pending gif here") : null,
      m("form", {onsubmit: utils.returnFalse}, [
        m("input", {placeholder: "name", onchange: m.withAttr("value", create.name), value: create.name()}),
        m("p", "a gist with a description.md file: 109ace31008190db7449"),
        m("input", {placeholder: "gist id", onchange: m.withAttr("value", create.gistId), value: create.gistId()}),
        m("input", {placeholder: "goal", onchange: m.withAttr("value", create.goal), value: create.goal()}),
        m("button", {onclick: create.submit}, "create campaign")
      ])
    ])
  ];
};

module.exports = create;
