var m = require("mithril"),
    utils = require("../utils"),
    marked = require("marked");

var campaign = {};

campaign.controller = function(c) {
  var self = this;
  this.c = c;
  this.contribution = m.prop();
  this.description = m.prop();
  this.funders = [];

  m.request({dataType: "jsonp", url: "https://api.github.com/gists/"+c.gistId}).then(this.description);

  campaign.watch = web3.eth.watch("chain");
  campaign.watch.changed(function(){
    self.funders = [];
    var numFunders = c.numFunders,
        currId = numFunders - 1;
    while (currId >= 0){
      self.funders[currId] = {
        id: currId,
        address: c.contract.funders(currId)[0],
        amount: c.contract.funders(currId)[1].toNumber()/utils.oneEtherInWei
      }
      currId--;
    }
    console.log(self.funders);
    m.redraw();
  });

  this.contribute = function(){
    var contribution = parseInt(parseFloat(self.contribution())*utils.oneEtherInWei);
    console.log("contributing amount:", contribution, c.contract.address);
    web3.eth.transact({from: web3.eth.coinbase, to: c.contract.address, value: contribution.toString()});
    // self.c.contract.value(contribution).transact().contribute();
  };

  this.collect = function(){
    self.c.contract.transact().collect();
  };

  this.refund = function(funderId){
    self.c.contract.transact().refund(funderId);
  }
};

campaign.view = function(ctrl) {
  var c = ctrl.c;
  return [
    m("main.dapp-content.campaign", [
      m("h1", c.name),
      ctrl.description() ? m.trust(marked(ctrl.description().data.files["description.md"].content)) : m("", "..."),
      m("p", "goal: " + c.goal),
      m("p", "number of funders: " + c.numFunders),
      m("p", "raised: " + c.raised),
      m("p", "balance: " + c.balance),
      m("p", "contract address: " + c.contract.address),
      web3.eth.coinbase === c.beneficiary && c.balance > 0 && c.raised >= c.goal ? m("button", {onclick: ctrl.collect} ,"collect") : null,
      m("form", {onsubmit: utils.returnFalse}, [
        m("input", {placeholder: "amount", type: "number", onchange: m.withAttr("value", ctrl.contribution)}),
        m("button", {onclick: ctrl.contribute}, "contribute")
      ]),
      m("table",[
        m("tbody", ctrl.funders.map(function(funder){
          return m("tr", [
            m("td", funder.address),
            m("td", funder.amount),
            funder.address === web3.eth.coinbase && c.raised < c.goal && funder.amount > 0 ?
              m("td", m("button", {onclick: ctrl.refund.bind(ctrl, funder.id)}, "refund")) : null
          ]);
        }))
      ])
    ])
  ];
};

module.exports = campaign;
