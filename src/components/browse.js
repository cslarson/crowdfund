var m = require("mithril");

var browse = {};

browse.controller = function(campaigns) {
  this.campaigns = campaigns.sort(function(a,b){return b.startTime - a.startTime});

  this.viewCampaign = function(c){
    m.route("/campaign/"+c.id);
  }
  // console.log("list campaigns controller")
};

browse.view = function(ctrl) {
  return [
    m("main.dapp-content.browse", [
      m("h1", "browse campaigns"),
      m("ul.row", Object.keys(ctrl.campaigns).map(function(campaignId){
        var c = ctrl.campaigns[campaignId];
        return m("li.campaign.simple.col.col-1-3", {onclick: ctrl.viewCampaign.bind(ctrl, c)}, [
          m(".inner", [
            m("h3", c.name),
            m("p", "goal: "+c.goal),
            m("p", "raised: "+c.raised)
          ])
        ]);
      }))
    ]),
    m("aside.dapp-actionbar", [
      m("h4", "filtering here? (keyword, time, goal, raised, etc.)")
    ])
  ];
};

module.exports = browse;
