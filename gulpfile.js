var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
//var reactify = require('reactify');
var watchify = require('watchify');
var browserify = require('browserify');
var watchLess = require('gulp-watch-less');
var less = require('gulp-less');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var bundler = watchify(browserify('./src/loader.js', watchify.args));
// add any other browserify options or transforms here
// bundler.transform('brfs');
// bundler.transform(reactify);

gulp.task('scripts', bundle); // so you can run `gulp js` to build the file

gulp.task('less', function () {
    return gulp.src('src/styles.less')
        // .pipe(watchLess('src/styles.less'))
        .pipe(less())
        .pipe(gulp.dest('dapp'));
});

bundler.on('update', bundle); // on any dep update, runs the bundler
bundler.on('log', gutil.log); // output build logs to terminal

function bundle() {
  return bundler.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('dapp.js'))
    // optional, remove if you dont want sourcemaps
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
    .pipe(sourcemaps.write('./')) // writes .map file
    //
    .pipe(gulp.dest('./dapp'));
}
